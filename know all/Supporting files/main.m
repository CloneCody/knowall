//
//  main.m
//  know all
//
//  Created by влад Филиппов on 25.09.15.
//  Copyright © 2015 Vlad Filipau. All rights reserved.
//

#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
