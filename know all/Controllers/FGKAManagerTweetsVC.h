//
//  ViewController.h
//  know all
//
//  Created by влад Филиппов on 25.09.15.
//  Copyright © 2015 Vlad Filipau. All rights reserved.
//

#import "FGKAUserStoryTVC.h"

extern NSString * const FGKAManagerTweetsVCAuthorizationViewText;
extern NSString * const FGKAManagerTweetsVCAuthorizationViewButtonText;
extern NSString * const FGKAManagerTweetsStoryBoardName;
extern NSString * const FGKAUserStoryTVCClassName;

@interface FGKAManagerTweetsVC : UIViewController <UITextFieldDelegate>

@property(nonatomic, strong) NSMutableArray *TWData;
- (IBAction)showUser:(id)sender;

@end

