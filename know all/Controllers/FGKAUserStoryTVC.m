//
//  FGKAUserStoryTVC.m
//  know all
//
//  Created by влад Филиппов on 01.10.15.
//  Copyright © 2015 Vlad Filipau. All rights reserved.
//

#import "FGKAUserStoryTVC.h"


NSString * const FGKAUserStoryTVCCellReuseIdentifier = @"tweetCell";

@interface FGKAUserStoryTVC () <UIViewControllerPreviewingDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tweetsTV;

@property(nonatomic, strong) NSMutableArray *TWDataInTweetFormat;


@property(nonatomic, strong) TWTRTweetView *twv;

@property (nonatomic, strong) UIButton *closeTweetB;

@end

@implementation FGKAUserStoryTVC

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareTVandTweets];
    [self prepareDeployedTweet];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.TWDataInTweetFormat.count;
}

- (TWTRTweetTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TWTRTweetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:FGKAUserStoryTVCCellReuseIdentifier
                                                                   forIndexPath:indexPath];
    [cell configureWithTweet:self.TWDataInTweetFormat[indexPath.row]];
    
     NSArray *subviews = [cell subviews];
    UIView *view = subviews[0];
    view.userInteractionEnabled = NO;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [TWTRTweetTableViewCell heightForTweet:self.TWDataInTweetFormat[indexPath.row]
                                            width:CGRectGetWidth(self.view.bounds)];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    [self.view addSubview:self.closeTweetB];
    self.closeTweetB.translatesAutoresizingMaskIntoConstraints = NO;
    [self createConstraintsAllScreen:self.closeTweetB];
    
    
    self.twv = [[TWTRTweetView alloc] initWithTweet:self.TWDataInTweetFormat[indexPath.row] style:TWTRTweetViewStyleRegular];
    [self.view addSubview:self.twv];
    self.twv.translatesAutoresizingMaskIntoConstraints = NO;
    [self createConstraintsTweet:self.twv];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
}

- (void)closeTweet {
    [self.closeTweetB removeFromSuperview];
    [self.twv removeFromSuperview];
}


#pragma mark - Preparation

- (void)prepareTVandTweets {
    self.tweetsTV.rowHeight = UITableViewAutomaticDimension;
    [self.tweetsTV registerClass:[TWTRTweetTableViewCell class] forCellReuseIdentifier:FGKAUserStoryTVCCellReuseIdentifier];
    
    self.TWDataInTweetFormat = [NSMutableArray array];
    for (NSDictionary *tweet in self.TWData) {
        [self.TWDataInTweetFormat addObject:[[TWTRTweet alloc] initWithJSONDictionary:tweet]];
    }
}

- (void)prepareDeployedTweet {
    self.closeTweetB = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.closeTweetB addTarget:self action:@selector(closeTweet) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark Constraints

- (void)createConstraintsTweet: (UIView*) view {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1
                                                           constant:0]];

}
- (void)createConstraintsAllScreen: (UIView*) view {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeRight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeRight
                                                         multiplier:1
                                                           constant:0]];
}

@end
