//
//  ViewController.m
//  know all
//
//  Created by влад Филиппов on 25.09.15.
//  Copyright © 2015 Vlad Filipau. All rights reserved.
//


#import "FGKAManagerTweetsVC.h"

NSString * const FGKAManagerTweetsVCAuthorizationViewText = @"go to settings to log in";
NSString * const FGKAManagerTweetsVCAuthorizationViewButtonText = @"Go to settings";
NSString * const FGKAManagerTweetsStoryBoardName = @"Main";
NSString * const FGKAUserStoryTVCClassName = @"IDuserStoryTVC";

@interface FGKAManagerTweetsVC ()
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortBy;
@property (weak, nonatomic) IBOutlet UISwitch *ascendingSwitch;

@property (strong, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UITextField *inputInfField;

@property (strong, nonatomic) UIVisualEffectView *blurVibrancyEffectViewAuthorization;

@property (strong, nonatomic) NSMutableDictionary* tableSearchWord;
@property (strong, nonatomic) NSMutableArray* tableSearchUser;
@property (strong, nonatomic) NSMutableArray* tableSearchData;
@property (strong, nonatomic) NSMutableArray* tableSearchTags;

@property (strong, nonatomic) NSArray* allTweet;

- (IBAction)addChoose:(id)sender;

- (void)showViewCheckAuthorization;

@end

@implementation FGKAManagerTweetsVC

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[FGKANetworkManager sharedInstance] startCheckReachability];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showViewCheckAuthorization)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:NULL];
    self.TWData = [[NSMutableArray alloc] init];
    
    self.tableSearchWord = [[NSMutableDictionary alloc] init];
    self.tableSearchUser = [NSMutableArray array];
    self.tableSearchData = [NSMutableArray array];
    self.tableSearchTags = [NSMutableArray array];
    
    [self.tableSearchWord setValue:self.tableSearchUser forKey:@"user"];
    [self.tableSearchWord setValue:self.tableSearchTags forKey:@"tag"];
    [self.tableSearchWord setValue:self.tableSearchData forKey:@"word"];
    
    [self.table setSectionIndexMinimumDisplayRowCount:2];
    
    self.inputInfField.delegate = self;
}

#pragma mark - methods work withk authorization

-(void)showViewCheckAuthorization {
    if ([[FGKANetworkManager sharedInstance] allAccounts].count == 0) {
        UIBlurEffect *blur= [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVibrancyEffect *vibrancy = [UIVibrancyEffect effectForBlurEffect:blur];
        
        self.blurVibrancyEffectViewAuthorization = [[UIVisualEffectView alloc] initWithEffect:blur];
        UIVisualEffectView *vibrancyEffectViewAuthorization = [[UIVisualEffectView alloc] initWithEffect:vibrancy];
        
        [self.view addSubview:self.blurVibrancyEffectViewAuthorization];
        [self.blurVibrancyEffectViewAuthorization addSubview:vibrancyEffectViewAuthorization];
        
        self.blurVibrancyEffectViewAuthorization.translatesAutoresizingMaskIntoConstraints = NO;
        [self createConstraintsBlurVibrancyEffects:self.blurVibrancyEffectViewAuthorization];
        
        vibrancyEffectViewAuthorization.translatesAutoresizingMaskIntoConstraints = NO;
        [self createConstraintsBlurVibrancyEffects:vibrancyEffectViewAuthorization];

        UILabel *rulesForNotLoggedUser = [[UILabel alloc] init];
        rulesForNotLoggedUser.translatesAutoresizingMaskIntoConstraints = NO;
        [vibrancyEffectViewAuthorization addSubview:rulesForNotLoggedUser];
        [self createConstraintsForRulesForNotLoggedUser:rulesForNotLoggedUser];
        
        rulesForNotLoggedUser.text = FGKAManagerTweetsVCAuthorizationViewText;
        rulesForNotLoggedUser.textAlignment =  NSTextAlignmentCenter;
        
        
        UIButton *openSettings = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        
        [openSettings setTitle:FGKAManagerTweetsVCAuthorizationViewButtonText forState:UIControlStateNormal];
        [openSettings addTarget:self action:@selector(openSettings) forControlEvents:UIControlEventTouchUpInside];
        [vibrancyEffectViewAuthorization addSubview:openSettings];
        
        openSettings.translatesAutoresizingMaskIntoConstraints = NO;
        [self createConstraintsForOpenSettings:openSettings];
    }
}

- (void)openSettings {
    [self.blurVibrancyEffectViewAuthorization removeFromSuperview];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

#pragma mark - IBAction

- (IBAction)showUser:(id)sender {
    if ([[FGKANetworkManager sharedInstance] checkInternetConnection] && self.tableSearchUser.count) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [[FGKANetworkManager sharedInstance] downloadTweetsByUser:self.tableSearchUser
                                                   withCompletion:^(NSArray *allTweets, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error.localizedDescription);
                return;
            } else {
    
               allTweets = [self filterTweets:allTweets];
                
                if (self.sortBy.selectedSegmentIndex == 0) {
                    self.TWData = (NSMutableArray*)[self sortDictionarysArray:allTweets
                                                                       Key:@"retweet_count"
                                                                 Ascending:self.ascendingSwitch.on];
                }
                if (self.sortBy.selectedSegmentIndex == 1) {
                    self.TWData = (NSMutableArray*)[self sortDictionarysArray:allTweets
                                                                          Key:@"favorite_count"
                                                                    Ascending:self.ascendingSwitch.on];
                }
               
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:FGKAManagerTweetsStoryBoardName bundle:nil];
                    FGKAUserStoryTVC *newViewController = [storyboard instantiateViewControllerWithIdentifier:
                                                           FGKAUserStoryTVCClassName];
                    
                    newViewController.TWData = self.TWData;
                    
                    [self presentViewController:newViewController animated:YES completion:nil];
                });
            }
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }];
    }
}

#pragma mark - Constraints

- (void)createConstraintsForRulesForNotLoggedUser: (UIView*) view {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:0.3
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:0
                                                           constant:20]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeRight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeRight
                                                         multiplier:1
                                                           constant:0]];
}

- (void)createConstraintsForOpenSettings: (UIView*) view {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:0.6
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:0
                                                           constant:20]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:0
                                                           constant:100]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1
                                                           constant:0]];
}
- (void)createConstraintsBlurVibrancyEffects: (UIView*) view {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeRight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeRight
                                                         multiplier:1
                                                           constant:0]];
}


- (NSArray *)sortDictionarysArray:(NSArray *)array Key:(NSString *)key Ascending:(bool)ascending {
    
    @try{
        NSArray *sortedArray;
        NSSortDescriptor *leagueDescriptor = [[NSSortDescriptor alloc] initWithKey:key
                                                                         ascending:ascending
                                                                          selector:nil];
        NSArray *descriptors = [NSArray arrayWithObjects:leagueDescriptor, nil];
        sortedArray = [array sortedArrayUsingDescriptors:descriptors];
        return sortedArray;
    }
    @catch(NSException *e){
        NSLog(@"Error!!");
    }
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.tableSearchWord allKeys] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [[self.tableSearchWord allKeys] objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.tableSearchWord objectForKey:[[self.tableSearchWord allKeys] objectAtIndex:section]] count];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [self.tableSearchWord allKeys];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    //Поиск ячейки
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //Если ячейка не найдена
    if (cell == nil) {
        //Создание ячейки
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2
                                      reuseIdentifier:CellIdentifier];
    }
    
    UIButton* childButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:childButton];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    NSString *word = [[self.tableSearchWord valueForKey:[[self.tableSearchWord allKeys] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    cell.textLabel.text = word;
    [cell.contentView addSubview:childButton];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *user = @"user";
    NSString *tag = @"tag";
    NSString *word = @"word";
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.table beginUpdates];
        NSString *title = [[self.tableSearchWord allKeys] objectAtIndex:indexPath.section];//or something similar to this based on your data source array structure
        //remove the corresponding object from your data source array before this or else you will get a crash
        
        if ([title isEqualToString:user]) {
            [self.tableSearchUser removeObjectAtIndex:indexPath.row];
        }
        
        if ([title isEqualToString:tag]) {
            [self.tableSearchTags removeObjectAtIndex:indexPath.row];
        }
        
        if ([title isEqualToString:word]) {
            [self.tableSearchData removeObjectAtIndex:indexPath.row];
        }
        
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.table endUpdates];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark - table

- (void)deleteChoose:(UIButton*)sender {
    [sender.superview removeFromSuperview];
}

- (IBAction)addChoose:(UIButton*)sender {
    NSString *user = @"add username";
    NSString *tag = @"add tag";
    NSString *word = @"add word";
    
    NSString* searchedWord = [_inputInfField text];
    searchedWord = [searchedWord stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" !?,.:;"]];
    
    if (![searchedWord isEqualToString:@""] && ![searchedWord isEqualToString:@" "]) {
        
        NSString *titleButton = sender.titleLabel.text;

        if ([titleButton isEqualToString:user]) {
            [self.tableSearchUser insertObject:searchedWord atIndex:0];
            [self addSearchedAttribute:(int)[[self.tableSearchWord allKeys] indexOfObject:@"user"]];
        }
        
        if ([titleButton isEqualToString:tag]) {
            [self.tableSearchTags insertObject:searchedWord atIndex:0];
            [self addSearchedAttribute:(int)[[self.tableSearchWord allKeys] indexOfObject:@"tag"]];
        }
        
        if ([titleButton isEqualToString:word]) {
            [self.tableSearchData insertObject:searchedWord atIndex:0];
            [self addSearchedAttribute:(int)[[self.tableSearchWord allKeys] indexOfObject:@"word"]]; 
        }
        
        [_inputInfField setText:@""];
    }
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [[self.tableSearchWord allKeys] indexOfObject:title];
}

- (void)addSearchedAttribute:(int)numberSection {
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:numberSection];
    [self.table beginUpdates];
    [self.table insertRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationLeft];
    [self.table endUpdates];
}

- (NSArray*)filterTweets:(NSArray*)allTweets {
    
    NSMutableArray *mutableTweets = [allTweets mutableCopy];
    
    for (NSInteger i=0; i<[mutableTweets count]; i++) {
        NSDictionary *dict = [mutableTweets objectAtIndex:i];
        
        NSUInteger numberOfMatches = 0;
       
        //remove all tweets with other tags(find at least one tag)
        
        for (NSString *tagWord in self.tableSearchTags) {
            
            NSDictionary *entities = [NSDictionary dictionaryWithDictionary:[dict objectForKey:@"entities"]];
            NSArray *hashtags = [NSArray arrayWithArray:[entities objectForKey:@"hashtags"]];
            NSMutableArray *hashtagsTexts = [NSMutableArray array];
            for (id hashtag in hashtags) {
                [hashtagsTexts addObject:[hashtag objectForKey:@"text"]];
            }

            for (NSString *tag in hashtagsTexts){
                //numberOfMatches = [regex numberOfMatchesInString:tag
                //                                            options:0
                //                                            range:NSMakeRange(0, [tag length])];
                
                if ([tagWord isEqualToString:tag]) {
                    numberOfMatches = 1;
                }
                
                if (numberOfMatches) { //find the same tag
                    break;
                }
            }
            if (numberOfMatches) { //find tag therefore find in next tweet
                break;
            }
        }
        
        //look over all tags in tweet and searched user's tags. if don't find searched tags, delete current and find in next tweet
        if (!numberOfMatches) {
            if ([self.tableSearchTags count]) {
                [mutableTweets removeObject:dict];
                i--;
            }
        }
    }
    for (NSInteger i=0; i<[mutableTweets count]; i++) {
        NSDictionary *dict = [mutableTweets objectAtIndex:i];
        
        NSError *error = nil;
        //remove all tweets with other's words
        for (NSString *word in self.tableSearchData) {
            //NSString *tagMatcher = [NSString stringWithFormat: @"*\\s%@",word];
            //NSString *tagMatcher = @"?(\\s)";
            //tagMatcher = [tagMatcher stringByAppendingString:word];
            //tagMatcher = [tagMatcher stringByAppendingString:@"\\s"];
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:word
                                                                                   options:NSRegularExpressionAllowCommentsAndWhitespace
                                                                                     error:&error];
            
            //NSRegularExpression *regexWord = [NSRegularExpression regularExpressionWithPattern:word
            //                                                              options:NSRegularExpressionCaseInsensitive
            //                                                              error:&error];
            
            
            NSUInteger numberOfMatchesWord = [regex numberOfMatchesInString:[dict objectForKey:@"text"]
                                                                    options:0
                                                                      range:NSMakeRange(0, [[dict objectForKey:@"text"] length])];
            if ( !numberOfMatchesWord ) { //don't find word in that tweet
                [mutableTweets removeObject:dict];
                i--;
            }
        }
    }
    return mutableTweets;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}



@end







