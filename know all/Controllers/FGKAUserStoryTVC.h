//
//  FGKAUserStoryTVC.h
//  know all
//
//  Created by влад Филиппов on 01.10.15.
//  Copyright © 2015 Vlad Filipau. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TwitterKit/TwitterKit.h>

extern NSString * const FGKAUserStoryTVCCellReuseIdentifier;

@interface FGKAUserStoryTVC : UIViewController <UITableViewDataSource, UITableViewDelegate, TWTRTweetViewDelegate>

@property(nonatomic, strong) NSMutableArray *TWData;

@end
