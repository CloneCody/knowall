//
//  FGKAnetworkManager.m
//  know all
//
//  Created by влад Филиппов on 15.10.15.
//  Copyright © 2015 Vlad Filipau. All rights reserved.
//

// show all tweet
//add constaints
//create coredata

#import "FGKANetworkManager.h"

NSString * const alertViewMessage = @"no internet connection";
NSString * const alertViewTitle = @"Sorry";


@implementation FGKANetworkManager

+ (instancetype)sharedInstance{
    static FGKANetworkManager *networkManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        networkManager = [[self alloc] init];
    });
    return networkManager;
}

#pragma mark - accounts

- (NSArray*)allAccounts {
    ACAccountStore *accountStore =[[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    NSArray *accounts = [accountStore accountsWithAccountType:accountType];
    return accounts;
}

#pragma mark - network

- (void)downloadTweetsByUser:(NSArray* )users withCompletion:(FetchTweetsCompletion) completion {
    ACAccountStore *accountStore =[[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
        if (granted) {
            NSArray *accounts = [accountStore accountsWithAccountType:accountType];
            ACAccount *twitterAccount = [accounts objectAtIndex:0];
            
            
             NSMutableArray *allTweets = [[NSMutableArray alloc] init];
            
            //
            
            __block int counter = 0;
            for (NSString *user in users) {

                __block int countPagesFlag = 1;
                __block NSMutableArray *allPagesTweets = [[NSMutableArray alloc] init];
                NSURL *statusesShowEndpoint = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/user_timeline.json"];
                
                for (int i = 1; i <= 18; i++) {
                
                    NSDictionary *params = @{@"include_entities": @"true",
                                             @"include_rts": @"true",
                                             @"screen_name": user,
                                             @"count": @"200",
                                             @"page": [NSString stringWithFormat:@"%i", i]};
                    

                    SLRequest *twitterRequest = nil;
                    
                    twitterRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                                   requestMethod:SLRequestMethodGET
                                                                             URL:statusesShowEndpoint
                                                                      parameters:params];
                    [twitterRequest setAccount:twitterAccount];
                    
                    [twitterRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                        if (responseData) {
                            [allPagesTweets addObject:[NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:nil]];
                            
                            if (countPagesFlag == 18) {
                                // iterate to remove empty              
                                for (NSArray *help in allPagesTweets) {
                                    [allTweets addObjectsFromArray:help];
                                }
                                
                                if (counter + 1 == users.count) {

                                    completion(allTweets, nil);
                                }
                                counter++;
                            }
                            
 
                            
                            countPagesFlag++;
                        } else {
                            //not once will bring many times the error
                            static dispatch_once_t taskForError;
                            dispatch_once(&taskForError, ^{
                                completion(nil, error);
                            });
                        }
                    }];
                }
            }
        }
    }];
}

#pragma mark - Reachability

- (void)startCheckReachability {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.internetReachable = [Reachability reachabilityForInternetConnection];
    [self.internetReachable startNotifier];
}

- (BOOL)checkInternetConnection {
    if ([self.internetReachable currentReachabilityStatus] == NotReachable) {
        return NO;
    } else {
        return YES;
    }
}

- (void)checkNetworkStatus:(NSNotification *)notice {
    if ([self.internetReachable currentReachabilityStatus] == NotReachable) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertViewTitle
                                                            message:alertViewMessage
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

@end




