//
//  FGKAnetworkManager.h
//  know all
//
//  Created by влад Филиппов on 15.10.15.
//  Copyright © 2015 Vlad Filipau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

extern NSString * const alertViewMessage;
extern NSString * const alertViewTitle;

@interface FGKANetworkManager : NSObject

typedef void(^FetchTweetsCompletion)(NSArray *, NSError *);

+(instancetype)sharedInstance;

- (void)downloadTweetsByUser:(NSArray* )users withCompletion:(FetchTweetsCompletion) completion;
- (NSArray*)allAccounts;

@property Reachability *internetReachable;

- (BOOL)checkInternetConnection;
- (void)startCheckReachability;

@end
