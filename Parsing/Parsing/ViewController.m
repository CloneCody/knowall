//
//  ViewController.m
//  Parsing
//
//  Created by Admin on 22.10.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UISearchBar *searchField;

@property (strong, nonatomic) NSMutableDictionary* tableSearchWord;

@property (strong, nonatomic) NSMutableArray* tableSearchUser;
@property (strong, nonatomic) NSMutableArray* tableSearchData;
@property (strong, nonatomic) NSMutableArray* tableSearchTags;

@property (strong, nonatomic) UIVisualEffectView *blurVibrancyEffectViewAuthorization;

@property (strong, nonatomic) NSArray* allTweet;


- (IBAction)addChoose:(id)sender;
@end

@implementation ViewController
@synthesize tableSearchData;
@synthesize tableSearchTags;
@synthesize allTweet;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //NSMutableArray *TWData = [[NSMutableArray alloc] init];
    //NSArray *allTweets = [[NSArray alloc] init];
    //tableSearchData = [NSMutableArray arrayWithObjects:@"One", @"Two", @"Tree", nil];
    //tableSearchTags = [NSMutableArray arrayWithObjects:@"Onetag", @"Twotag", @"Treetag", nil];
    
    
    NSMutableDictionary* tweet = [[NSMutableDictionary alloc] init];
    NSArray* hashtags = [NSArray arrayWithObjects:@"tag1", @"tag2", nil];
    NSString* textTweet = @"HelloWord, #tag1";
    [tweet setValue:hashtags forKey:@"hashtags"];
    [tweet setValue:textTweet forKey:@"text"];
    NSMutableDictionary* tweet2 = [[NSMutableDictionary alloc] init];
    NSArray* hashtags2 = [NSArray arrayWithObjects:@"tag3", @"tag4", nil];
    NSString* textTweet2 = @"Hello, guest";
    [tweet2 setValue:hashtags2 forKey:@"hashtags"];
    [tweet2 setValue:textTweet2 forKey:@"text"];
    allTweet = [NSArray arrayWithObjects:tweet, tweet2, nil];
    NSLog(@"Count all tweets %fl", (float)[allTweet count]);
    
    
    self.tableSearchWord = [[NSMutableDictionary alloc] init];
    self.tableSearchUser = [NSMutableArray array];
    self.tableSearchData = [NSMutableArray array];
    self.tableSearchTags = [NSMutableArray array];
    
    [self.tableSearchWord setValue:self.tableSearchUser forKey:@"user"];
    [self.tableSearchWord setValue:self.tableSearchTags forKey:@"tag"];
    [self.tableSearchWord setValue:self.tableSearchData forKey:@"word"];
    
    [self.table setSectionIndexMinimumDisplayRowCount:2];
}

- (IBAction)filter:(id)sender{
    NSLog(@"Count all tweets %fl", (float)[allTweet count]);
    
    allTweet = [self filterTweets:allTweet];
    
    NSLog(@"Count all tweets %fl", (float)[allTweet count]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSArray*)filterTweets:(NSArray*)allTweets {
    
    NSMutableArray *mutableTweets = [allTweets mutableCopy];
    
    for (NSInteger i=0; i<[mutableTweets count]; i++) {
        NSDictionary *dict = [mutableTweets objectAtIndex:i];
        
        NSUInteger numberOfMatches = 0;
        NSError *error = NULL;
        
        //remove all tweets with other tags(find at least one tag)
        
        for (NSString *tagWord in tableSearchTags) {
            
            NSArray *hashtags = [NSArray arrayWithArray:[dict objectForKey:@"hashtags"]];
        
            for (NSString *tag in hashtags){
                //numberOfMatches = [regex numberOfMatchesInString:tag
                //                                            options:0
                //                                            range:NSMakeRange(0, [tag length])];
            
                if ([tagWord isEqualToString:tag]) {
                    numberOfMatches = 1;
                }
                
                if (numberOfMatches) { //find the same tag
                    break;
                }
            }
            if (numberOfMatches) { //find tag therefore find in next tweet
                break;
            }
        }
        
        //look over all tags in tweet and searched user's tags. if don't find searched tags, delete current and find in next tweet
        if (!numberOfMatches) {
            if ([tableSearchTags count]) {
                [mutableTweets removeObject:dict];
                i--;
            }
        }
        
        //remove all tweets with other's words
        for (NSString *word in tableSearchData) {
            //NSString *tagMatcher = [NSString stringWithFormat: @"*\\s%@",word];
            //NSString *tagMatcher = @"?(\\s)";
            //tagMatcher = [tagMatcher stringByAppendingString:word];
            //tagMatcher = [tagMatcher stringByAppendingString:@"\\s"];
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:word
                                                                                options:NSRegularExpressionAllowCommentsAndWhitespace
                                                            error:&error];
            
            //NSRegularExpression *regexWord = [NSRegularExpression regularExpressionWithPattern:word
            //                                                              options:NSRegularExpressionCaseInsensitive
            //                                                              error:&error];
                        
                
            NSUInteger numberOfMatchesWord = [regex numberOfMatchesInString:[dict objectForKey:@"text"]
                                                                    options:0
                                                                      range:NSMakeRange(0, [[dict objectForKey:@"text"] length])];
            if ( !numberOfMatchesWord ) { //don't find word in that tweet
                [mutableTweets removeObject:dict];
                i--;
            }
        }
    }
    return mutableTweets;
}


#pragma mark - table

- (void)deleteChoose:(UIButton*)sender {
    [sender.superview removeFromSuperview];
}

- (IBAction)addChoose:(UIButton*)sender {
    NSString *user = @"add username";
    NSString *tag = @"add tag";
    NSString *word = @"add word";
    
    NSLog(@"%fl", (float)[[self.tableSearchWord objectForKey:[[self.tableSearchWord allKeys] objectAtIndex:0]] count]);
    
    NSString* searchedWord = [_searchField text];
    searchedWord = [searchedWord stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" !?,.:;"]];
    
    if (![searchedWord isEqualToString:@""] && ![searchedWord isEqualToString:@" "]) {
        
        NSString *titleButton = sender.titleLabel.text;
        if ([titleButton isEqualToString:user]) {
            [self.tableSearchUser insertObject:searchedWord atIndex:0];
            [self addSearchedAttribute:2];
        }
        
        if ([titleButton isEqualToString:tag]) {
            [self.tableSearchTags insertObject:searchedWord atIndex:0];
            [self addSearchedAttribute:0];
        }
        
        if ([titleButton isEqualToString:word]) {
            [self.tableSearchData insertObject:searchedWord atIndex:0];
            [self addSearchedAttribute:1];
        }
        
        [_searchField setText:@""];
        
        for (NSString* words in self.tableSearchData) {
            NSLog(@"%@", words);
        }
        
        
        for (NSString* words in self.tableSearchUser) {
            NSLog(@"%@", words);
        }
    }
}

-(void)addSearchedAttribute:(int)numberSection
{
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:numberSection];
    [self.table beginUpdates];
    [self.table insertRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationLeft];
    [self.table endUpdates];
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.tableSearchWord allKeys] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[self.tableSearchWord allKeys] objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.tableSearchWord objectForKey:[[self.tableSearchWord allKeys] objectAtIndex:section]] count];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [self.tableSearchWord allKeys];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    //Поиск ячейки
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //Если ячейка не найдена
    if (cell == nil) {
        //Создание ячейки
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2
                                      reuseIdentifier:CellIdentifier];
    }
    
    UIButton* childButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:childButton];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    NSString *word = [[self.tableSearchWord valueForKey:[[self.tableSearchWord allKeys] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    cell.textLabel.text = word;
    [cell.contentView addSubview:childButton];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *user = @"user";
    NSString *tag = @"tag";
    NSString *word = @"word";
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.table beginUpdates];
        NSString *title = [[self.tableSearchWord allKeys] objectAtIndex:indexPath.section];//or something similar to this based on your data source array structure
        //remove the corresponding object from your data source array before this or else you will get a crash
        
        if ([title isEqualToString:user]) {
            [self.tableSearchUser removeObjectAtIndex:indexPath.row];
        }
        
        if ([title isEqualToString:tag]) {
            [self.tableSearchTags removeObjectAtIndex:indexPath.row];
        }
        
        if ([title isEqualToString:word]) {
            [self.tableSearchData removeObjectAtIndex:indexPath.row];
        }

        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.table endUpdates];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
@end
