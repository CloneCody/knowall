//
//  ViewController.m
//  projekt1
//
//  Created by Admin on 02.10.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
        
    UIButton *b1 = [UIButton buttonWithType:UIButtonTypeCustom];
    b1.frame = CGRectMake(30, 330, 100, 30);
    [b1 setTitle:@"Button 1" forState:UIControlStateNormal];
    [b1 setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    [b1 setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [b1 addTarget:self action:@selector(buttonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:b1];
    
    UIButton *b2 = [UIButton buttonWithType:UIButtonTypeCustom];
    b2.frame = CGRectMake(30, 350, 100, 30);
    [b2 setTitle:@"Button 2" forState:UIControlStateNormal];
    [b2 setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    [b2 setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [b2 addTarget:self action:@selector(buttonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:b2];
    
    UIButton *b3 = [UIButton buttonWithType:UIButtonTypeCustom];
    b3.frame = CGRectMake(30, 370, 100, 30);
    //b.backgroundColor = [UIColor greenColor];
    [b3 setTitle:@"Button 3" forState:UIControlStateNormal];
    [b3 setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    [b3 setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [b3 addTarget:self action:@selector(keyboardShow) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:b3];
}

-(void)buttonTapped{
    NSLog(@"lalala");
}

-(void)keyboardShow{
    NSLog(@"blabbbb");
    
}

@end
