//
//  ViewController.h
//  TwitterSearch
//
//  Created by Admin on 01.10.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITextField* searchField;

- (IBAction)addChoose:(id)sender;

- (IBAction)deleteChoose:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) NSMutableArray* tableSearchData;


//- (void) addChoose: (NSString*)word;
//- (void) deleteChoose;

@end

