//
//  ViewController.m
//  TwitterSearch
//
//  Created by Admin on 01.10.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "SearchController.h"

@interface SearchController () {
    CALayer* tableLayer;
}

@end

@implementation SearchController
@synthesize table;
@synthesize tableSearchData;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //[tableSearchData init];
    //[self createTableLayer];
    
    NSString* word = @"One";
    tableSearchData = [NSMutableArray arrayWithObjects:@"One", @"Two", @"Tree", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)deleteChoose:(UIButton*)sender{
    
    //[sender.superview removeFromSuperview];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableSearchData count];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [table beginUpdates];
        [tableSearchData removeObjectAtIndex:indexPath.row];//or something similar to this based on your data source array structure
        //remove the corresponding object from your data source array before this or else you will get a crash
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [table endUpdates];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    //Поиск ячейки
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //Если ячейка не найдена
    if (cell == nil) {
        //Создание ячейки
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2
                                       reuseIdentifier:CellIdentifier];
    }
    
    UIButton* childButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    //childButton.frame = CGRectMake(0, 0, cell.frame.size.height-3, cell.frame.size.height-3);
    //[childButton setTitle:@"-" forState:UIControlStateNormal];
    //[childButton addTarget:self action:@selector(deleteChoose:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:childButton];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    cell.textLabel.text = [tableSearchData objectAtIndex:indexPath.row];
    [cell.contentView addSubview:childButton];
    
    return cell;
}

- (void)createTableLayer{
    tableLayer = [CALayer layer];
    tableLayer.shadowOffset = CGSizeMake(0, 3);
    tableLayer.shadowRadius = 2.0;
    tableLayer.shadowColor = [UIColor blackColor].CGColor;
    tableLayer.shadowOpacity = 0.4;
    tableLayer.borderWidth = 1.0;
    tableLayer.cornerRadius = 3.0;
    tableLayer.frame = CGRectMake(0, 25, self.view.frame.size.width, 170);
    
    [self.view.layer addSublayer:tableLayer];
    
    
    tableLayer.contents = table;
    tableLayer.contentsGravity = kCAGravityCenter;
}

-(void)addWord
{
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    [table beginUpdates];
    [table insertRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationTop];
    [table endUpdates];
}

- (IBAction)addChoose:(id)sender {
    NSString* word = [_searchField text];
    if (![word isEqualToString:@""] && ![word isEqualToString:@" "]) {
        
        //if (tableLayer == nil) {
          //  [self createTableLayer];
        //}
        
        //_table.frame = CGRectMake(0, 0, _tableLayer.frame.size.width, _tableLayer.frame.size.height);
        
        [tableSearchData insertObject:word atIndex:0];
        for (NSString* words in tableSearchData) {
            NSLog(@"%@", words);
        }
        
        [self addWord];
    }
    
    //add Constraints (Autolayout)
}
@end
